// #include "AmgSolver.hpp"
#include "main.hpp"


int main()
{
    int row_, col_, nnz_;

    vector<int> rr;
    vector<int> cc;
    vector<double> aa;
    vector<double> bb;

    // Problem 1 4x4 matrix
    string FileName1 = "A_matrix.mtx";
    string FileName2 = "B_matrix.mtx";

    // string FileName1 = "gr_30_30_mat.mtx";
    // string FileName2 = "gr_30_30_rhs.mtx";
    ifstream inputMatData(FileName1);
    ifstream inputRhsData(FileName2);

    
    // ======================================================== //




    inputMatData >> row_ >>col_ >> nnz_;

    rr.resize(nnz_);
    cc.resize(nnz_);
    aa.resize(nnz_);
    bb.resize(row_);

    //read "A_matrix.mtx"
    for(int i=0; i<nnz_; ++i)
    {
        inputMatData >> rr[i] >> cc[i] >> aa[i];
    }

    //read "B_matrix.mtx"
    for(int i=0; i<row_; ++i)
    {   
        inputRhsData >>  bb[i];
    }



    // ======================================================== //
    CAMG_GenerateCoarseProblem();  // Run Setup phase  
    // ======================================================== //
}