# Makefile 

# Nom du compilateur
CC = g++

# Options de compilation: optimisation, debug etc...
OPT = -std=c++14 -march=native -mcmodel=large 
Linking = 
INC = #-I Header_Files/
# -ipo -xCORE-AVX2 -align array32byte 
# -mcmodel=large
# -heap-arrays 64 
# 

EXE = solver

DIR_OBJ = ./obj
DIR_BIN = ./bin

# Defining the objects (OBJS) variables  P.S. If u want to add cpp file, u can modify here.
OBJS =  \
    AmgSolver.o \


# Linking object files
exe :  $(OBJS) 
	$(CC) -o $(EXE) \
    $(OBJS) \
    $(OPT) $(Linking)


# echo something
	@echo "   ***************** successful *****************   "                                                                                      
	@echo "    |  Author:  LucaWei                        	   "                       
	@echo "    |  Version: 0.1                                 "                   
	@echo "    |  Editor : LucaWei                             "  

# Defining the flags of objects
%.o: %.cpp
	@$(CC) $< $(OPT) $(INC) -c 


# Removing object files
clean :
	@/bin/rm -f Output/*.dat
	@/bin/rm -f Output/*.x
	@/bin/rm -f Output/*.q

cleanall : 
	@/bin/rm -f $(OBJS) $(EXE)  *.mod
	@/bin/rm -f Output/*.dat
	@/bin/rm -f Output/*.x
	@/bin/rm -f Output/*.q
    
config :
	if [ ! -d obj ] ; then mkdir Output ; fi
